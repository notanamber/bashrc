# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples
# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# pgup binded to history search
bind '"\e[A": history-search-backward'

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
   PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
   # user: cyan
   #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;36m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

if [ -a ~/.pythonrc.py ]; then
    alias py='python -i -c "from user import *"'
fi

if [ -a ~/.py3rc.py ]; then
    export PYTHONSTARTUP=~/.py3rc.py
    alias py3='python3 -i'
fi

# some more ls aliases
alias 77='cd ~/gitolite/wubook/src'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'


export VISUAL=vim
export EDITOR="$VISUAL"

# 'rm -i' chiede conferma per l'eliminazione di ogni file
alias rm='rm -I'                    

# vim alias
alias vim='vim -p'
# it depends where you have installed nvim
alias nvim='~/nvim-linux64/bin/nvim -p'

# vim alias
alias wgetpage='wget --recursive --page-requisites --html-extension --convert-links'

# psql alias
alias lpsql='psql --host=192.168.1.253 -d foo -U foo -W'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias errecho='echo $1 1>&2'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# GIT
gitpush() {
   git pull
   git commit -a -m ${1-go}
   git push
}

swp() {
  myhelp() {
    echo 'This tool is usefull to remove .swp files'
    echo 'Usage: swp [-d dir] [-noi]'
    echo 'if you do not specify the directory in -d param, search starts from the current'
    echo 'if you do not specify the -noi param files removing is interactive'
  }
  if [ "$1" == '-h' ]; then
    myhelp
    return 1
  fi
  cpath=.
  interactive='-i'
  while [ -n "$1" ]; do # while loop starts
    case "$1" in 
      -noi) interactive='';;
      -d) if [[ -d "$2" ]]; then 
            cpath="$2"
          else
            echo Folder "$2" not exists
          fi
          shift;;
      *) echo "Option $1 not recognized" ;;
    esac
    shift #shift params
  done

  find $cpath -name "*."swp -exec rm $interactive {} \; 
}

purgefext() {
  myhelp() {
    echo 'This tool is usefull to remove specific files type'
    echo 'Usage: purgefext -e ext [-d dir] [-noi]'
    echo 'the e param is mandatory: is the files extension you want to remove'
    echo 'if you do not specify the directory in -d param, search starts from the current'
    echo 'if you do not specify the -noi param files removing is interactive'
  }
  if [ "$1" == '-h' ]; then
    myhelp
    return 1
  fi
  cpath=.
  interactive='-i'
  ext=''
  while [ -n "$1" ]; do # while loop starts
    case "$1" in 
      -noi) interactive='';;
      -d) if [[ -d "$2" ]]; then 
            cpath="$2"
          else
            echo Folder "$2" not exists
          fi
          shift;;
      -e) ext="$2"
          shift;;
      *) echo "Option $1 not recognized" ;;
    esac
    shift #shift params
  done
  
  if [ "$ext" == '' ]; then 
    myhelp
    return 1
  fi

  find $cpath -name "*."$ext -exec rm $interactive {} \; 
}


man() {
	env \
		LESS_TERMCAP_mb=$(printf "\e[1;31m") \
		LESS_TERMCAP_md=$(printf "\e[1;31m") \
		LESS_TERMCAP_me=$(printf "\e[0m") \
		LESS_TERMCAP_se=$(printf "\e[0m") \
		LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
		LESS_TERMCAP_ue=$(printf "\e[0m") \
		LESS_TERMCAP_us=$(printf "\e[1;32m") \
			man "$@"
}

gitdelbranch() {
  git branch -D $1
  git push origin :$1
}

gitopendiffs() {
  myhelp() {
    echo 'This tool is usefull to open in vim the diff with a branch and a branch in origin'
    echo 'Before to launch place in the repository'
  }
  if [ "$1" == '-h' ]; then
    myhelp
    return 1
  fi
  if [ $1 == '']; then
    branch='master'
  else
    branch=$1
  fi
  vim -p `git diff --name-only 'origin/'$branch | sed 's/src\///g'`
}

zlessc() {
  zless -R $1
}

export PATH=~/bin/:$PATH
